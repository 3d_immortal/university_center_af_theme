# The UniversityCenterAF VS-Code Theme

## About
A color theme that I created for Eclipse back in early 2012, and I've been using
since. I'm so used to it that I can't look comfortably at anything else.

Unfortunately, at the moment vscode doesn't support semantic highlighting, hence
this color theme looks far from what I like and used to, but it gets me as close
as possible to it.

To illustrate the difference, here's how it looks on Eclipse:

![Eclipse](/images/eclipse.png)

but sadly, this is how it currently looks on vscode:

![vscode](/images/vscode.png)

Close enough but far from what I want.